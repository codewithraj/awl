from django.http import cookie, response
from django.shortcuts import render
from datetime import datetime, timedelta



#secured+_ccokies
# Create your views here.
def setCookie(request):
    response =  render(request,'login/setCookie.html')
    response.set_signed_cookie(key='name',value='Raj & Devi',salt="salt",expires=datetime.utcnow()+timedelta(days=4))
    return response

def getCookie(request):
    name = request.get_signed_cookie(key='name',default = 'Guest User',salt='salt') #salt and ke match is mandatory
    return render(request,'login/getCookie.html',context={'nm':name})

def deleteCookie(request):
    response = render(request,'login/deleteCookie.html')
    response.delete_cookie(key='name')
    return response
