from django.http import cookie, response
from django.shortcuts import render
from datetime import datetime, timedelta


# Create your views here.
def setCookie(request):
    response =  render(request,'login/setCookie.html')
    response.set_cookie(key='name',value='Raj & Devi',expires=datetime.utcnow()+timedelta(days=4))
    return response

def getCookie(request):
    # name = request.COOKIES["name"]   # Alternate method below one
    name = request.COOKIES.get('name','sorry no cookies availables')
    return render(request,'login/getCookie.html',context={'nm':name})

def deleteCookie(request):
    response = render(request,'login/deleteCookie.html')
    response.delete_cookie(key='name')
    return response
