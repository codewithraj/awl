from django.http import response
from django.urls.base import resolve
from django.shortcuts import HttpResponse

class BrotherMW:

    def __init__(self,get_response):
        self.get_response = get_response
        print("one time init Brother")
    
    def __call__(self,request):
        print("This Brother code is executing BEFORE view call")
        response = self.get_response(request)
        print("it's Brother gonna execute AFTER view call ")
        return response

    def process_view(self,request,*args,**kwargs):
        print("this is process view of BrotherMW")
        # return HttpResponse("Khatam by Brother . Tata Bye !!!!")
        return None

class SisterMW:

    def __init__(self,get_response):
        self.get_response = get_response
        print("one time init Sister")
    
    def __call__(self,request):
        print("This Sister code is executing BEFORE view call")
        response = self.get_response(request)
        print("it's Sister gonna execute AFTER view call ")
        return response

class MotherMW:

    def __init__(self,get_response):
        self.get_response = get_response
        print("one time init Mother")
    
    def __call__(self,request):
        print("This Mother code is executing BEFORE view call")
        response = self.get_response(request)
        """
        suppose in some cases mother doesn't give you permission to access , 
        then without ecxecution of views it can return respose to end process further.
        it'll not let next processes execute
        """
        # response = HttpResponse("Pehli fursat pe nikal")
        print("it's Mother gonna execute AFTER view call ")
        return response

class FatherMW:

    def __init__(self,get_response):
        self.get_response = get_response
        print("one time init Father")
    
    def __call__(self,request):
        print("This Father code is executing BEFORE view call")
        response = self.get_response(request)
        print("it's Father gonna execute AFTER view call ")
        return response

def MomMW(get_response):
    #init of config - one time 
    print("init process started")
    def function(request):
        #before view/next middleware process
        print("before view")
        response =  get_response(request)     #calling next mw/view
        print("after view")
        return response
    return function   

class MomMW2:
    def __init__(self,get_response):
        self.get_resposne = get_response
        print("configs")

    def __call__(self,request):
        #before view/next middleware process
        print("before view")
        response =  self.get_response(request)     #calling next mw/view
        print("after view")
        return response

    
