from django.http import response
from django.urls.base import resolve
from django.shortcuts import HttpResponse

class ProcessViewMW:

    def __init__(self,get_response):
        self.get_response = get_response
        print("one time init ProcessViewMW")
    
    def __call__(self,request):
        print("This ProcessViewMW code is executing BEFORE view call")
        response = self.get_response(request)
        print("it's ProcessViewMW gonna execute AFTER view call ")
        return response
# process_view() is called just before Django calls the view.
    def process_view(self,request,*args,**kwargs):
        print("this is process view of ProcessViewMW")
        # return HttpResponse("Khatam by ProcessView . Tata Bye !!!!")
        return None

class ProcessExceptionMW:

    def __init__(self,get_response):
        self.get_response = get_response
        print("one time init ProcessExceptionMW")
    
    def __call__(self,request):
        print("This ProcessExceptionMW code is executing BEFORE view call")
        response = self.get_response(request)
        print("it's ProcessExceptionMW gonna execute AFTER view call ")
        return response
    # Django calls process_exception()
    def process_exception(self,request,exception,*args,**kwargs):
        print("inside process_exception")
        className = exception.__class__.__name__
        print(className)
        # return None
        return HttpResponse(f"Khatam by ProcessException \n Reason : {exception}. \n Tata Bye !!!")

class ProcessTemplateMW:

    def __init__(self,get_response):
        self.get_response = get_response
        print("one time init ProcessTemplateMW")
    
    def __call__(self,request):
        print("This ProcessTemplateMW code is executing BEFORE view call")
        response = self.get_response(request)
        """
        suppose in some cases mother doesn't give you permission to access , 
        then without ecxecution of views it can return respose to end process further.
        it'll not let next processes execute
        """
        # response = HttpResponse("Pehli fursat pe nikal")
        print("it's Mother gonna execute AFTER view call ")
        return response
    
    def process_template_response(self,request,response):
        print("process template response")
        response.context_data['name'] = 'Devi'
        return response

class FatherMW:

    def __init__(self,get_response):
        self.get_response = get_response
        print("one time init Father")
    
    def __call__(self,request):
        print("This Father code is executing BEFORE view call")
        response = self.get_response(request)
        print("it's Father gonna execute AFTER view call ")
        return response
        