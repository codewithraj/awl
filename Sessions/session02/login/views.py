from django.shortcuts import render
from django.views.generic import CreateView
# Create your views here.

def setSession(request):
    request.session['gf'] = 'Kali'
    request.session['bf'] = 'Mata'
    return render(request,'login/setSession.html')

def getSession(request):
    items = request.session.items()
    keys = request.session.keys()
    values = request.session.values()
    context = {'items':items, 'keys':keys, 'values':values}
    return render(request,'login/getSession.html',context=context)

def deleteSession(request):
    if not request.session._session:  #chcek for sesssion existance
        msg = 'Unsuccessfull'
        return render(request,'login/delSession.html',context={'msg':msg})
    request.session.flush()
    msg = 'Success'
    return render(request,'login/delSession.html',{'msg':msg})




