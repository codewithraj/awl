from django.shortcuts import render

# Create your views here.
def setSession(request):
    request.session['name'] = 'Devi'
    return render(request,'login/setSession.html')

def getSession(request):
    # val = request.session['name']
    val = request.session.get('name',default='Guest')
    context = {'name':val}
    return render(request,'login/getSession.html',context=context)

def delSession(request):
    if 'name' in request.session:
        print("yes found")
        del request.session['name']
    return render(request,'login/delSession.html')